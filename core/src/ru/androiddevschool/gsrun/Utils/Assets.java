package ru.androiddevschool.gsrun.Utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;

/**
 * Created by ga_nesterchuk on 04.03.2017.
 */

public class Assets {
    private static Assets ourInstance = new Assets();

    public static Assets get() {
        return ourInstance;
    }

    private Assets() {
        initImages();
        initFonts();
        initStyles();
    }

    private void initFonts() {
        fonts = new HashMap<String, BitmapFont>();

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("fonts/astron.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size = 30;
        parameter.color = Color.BLUE;
        fonts.put("score", generator.generateFont(parameter));

        fonts.put("simple", new BitmapFont());

    }

    private void initStyles() {
        buttonStyles = new HashMap<String, Button.ButtonStyle>();
        buttonStyles.put("char button",
                new Button.ButtonStyle(
                        images.get("Персонаж"),
                        images.get("Персонаж"),
                        null
                ));
        buttonStyles.put("play button",
                new Button.ButtonStyle(
                        images.get("Играть"),
                        images.get("Играть"),
                        null
                ));
        buttonStyles.put("settings button",
                new Button.ButtonStyle(
                        images.get("Настройки"),
                        images.get("Настройки"),
                        null
                ));

        buttonStyles.put("exit button",
                new Button.ButtonStyle(
                        images.get("Закрыть"),
                        images.get("Закрыть"),
                        null
                ));
    }

    private void initImages() {
        images = new HashMap<String, TextureRegionDrawable>();
        addImagesFolder("/images");

    }

    public void addImagesFolder(String folderName) {
        FileHandle file = Gdx.files.local(folderName);
        for (FileHandle f : file.list()) {
            if (f.name().endsWith("png"))
                images.put(f.nameWithoutExtension(), getDrawable(f));
        }
    }

    public TextureRegionDrawable getDrawable(FileHandle file) {
        return new TextureRegionDrawable(new TextureRegion(new Texture(file)));
    }

    public HashMap<String, TextureRegionDrawable> images;
    public HashMap<String, Button.ButtonStyle> buttonStyles;
    public HashMap<String, BitmapFont> fonts;
}