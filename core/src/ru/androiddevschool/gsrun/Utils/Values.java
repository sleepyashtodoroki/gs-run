package ru.androiddevschool.gsrun.Utils;

import com.badlogic.gdx.Gdx;

/**
 * Created by 01k1102 on 22.04.2017.
 */
public class Values {
    public static final float WORLD_WIDTH = 960;
    public static final float WORLD_HEIGHT = 540;
    public static final float ppuX = 1f*Gdx.graphics.getWidth()/WORLD_WIDTH;
    public static final float ppuY = 1f*Gdx.graphics.getHeight()/WORLD_HEIGHT;
    public static final float playerX = 50;
    public static final float playerY = 38;
    public static final float vX = 400; //скорость вправо
    public static final float dVy = 500; //скорость подскока
    public static final float sVy = 30; //скорость остановки
    public static final float g = 700; //скорость падения
    public static final float ground = 70; //высота земли
}
