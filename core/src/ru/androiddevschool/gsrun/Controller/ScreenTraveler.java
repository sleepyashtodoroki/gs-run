package ru.androiddevschool.gsrun.Controller;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

import ru.androiddevschool.gsrun.GSGame;

/**
 * Created by 01k1102 on 01.04.2017.
 */
public class ScreenTraveler extends ClickListener {
    private String name;
    public ScreenTraveler(String name){
        this.name = name;
    }
    public void clicked(InputEvent event, float x, float y) {//куда кнопка будет переводить
        GSGame.get().setScreen(name);
    }
}
