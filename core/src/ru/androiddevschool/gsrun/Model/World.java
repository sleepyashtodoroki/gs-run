package ru.androiddevschool.gsrun.Model;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.viewport.Viewport;

import ru.androiddevschool.gsrun.Utils.Assets;

/**
 * Created by 01k1102 on 08.04.2017.
 */
public class World extends Stage {
    private Player player;
    private float score;

    public World(Viewport viewport, Batch batch) {
        super(viewport, batch);
        TextureRegion[] frames = new TextureRegion[3];
        for (int i = 0; i < frames.length; i++)
            frames[i] = Assets.get().images.get("char0-run-" + i).getRegion();
        player = new Player(frames, 0.5f);
        player.setSize(150f);
        this.score = 0;
        addActor(player);
    }

    public Player getPlayer() {
        return player;
    }

    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
        super.touchDown(screenX, screenY, pointer,button);
        player.tap();
        return true;
    }


    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        super.touchUp(screenX, screenY, pointer, button);
        player.stopTap();
        return true;
    }

    public int getScore() {
        System.out.println(score);
        return (int) score;
    }

    public void addScore(float score) {
        this.score += score;
    }

    public void setScore(float score) {
        this.score = score;
    }
}
