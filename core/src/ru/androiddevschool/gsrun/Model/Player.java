package ru.androiddevschool.gsrun.Model;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

import ru.androiddevschool.gsrun.Utils.Values;

/**
 * Created by 01k1102 on 22.04.2017.
 */
public class Player extends Image {
    private Animation animation;
    private float animationTime;
    private Vector2 velocity;

    public Player(TextureRegion[] frames, float animationDuration) {
        super(new TextureRegionDrawable(frames[0]));
        this.animation = new Animation(animationDuration / frames.length, new Array<TextureRegion>(frames), Animation.PlayMode.LOOP_PINGPONG);
        this.animationTime = 0;
        velocity = new Vector2(Values.vX, 0);
        setPosition(Values.playerX, Values.playerY);
    }

    public void act(float delta) {
        super.act(delta);
        animationTime += delta;
        ((TextureRegionDrawable) getDrawable()).setRegion(animation.getKeyFrame(animationTime, true));

        if (getY() != Values.playerY) velocity.y -= Values.g * delta;
        moveBy(velocity.x * delta, velocity.y * delta);
        if (getY() < Values.playerY) {
            velocity.y = 0;
            setY(Values.playerY);
        }
        ((World) getStage()).setScore(animationTime);
    }

    public void tap() {
        if (velocity.y == 0) velocity.y = Values.dVy;
    }

    public void stopTap() {
        if (velocity.y > Values.sVy) velocity.y = Values.sVy;
    }

    public void setSize(float size) {
        setWidth(getWidth() / getHeight() * size);
        setHeight(size);
    }
}
