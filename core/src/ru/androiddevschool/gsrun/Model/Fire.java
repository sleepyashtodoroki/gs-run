package ru.androiddevschool.gsrun.Model;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;

import ru.androiddevschool.gsrun.Utils.Values;

/**
 * Created by 01k1102 on 29.04.2017.
 */
public class Fire extends Image {
    private Animation animation;
    private float animationTime;
    private float animationDuration;

    public Fire(TextureRegion[] frames, float animationDuration, float playerX) {
        super(new TextureRegionDrawable(frames[0]));
        this.animationDuration = animationDuration;
        this.animation = new Animation(animationDuration / frames.length, new Array<TextureRegion>(frames), Animation.PlayMode.LOOP_PINGPONG);
        this.animationTime = 0;
        setPosition(playerX+Values.WORLD_WIDTH, Values.playerY);
    }

    public void act(float delta) {
        super.act(delta);
        animationTime += delta;
        ((TextureRegionDrawable) getDrawable()).setRegion(animation.getKeyFrame(animationTime, true));

        ((World) getStage()).addScore(delta);
    }

    public void setSize(float size) {
        setWidth(getWidth() / getHeight() * size);
        setHeight(size);
    }
}
