package ru.androiddevschool.gsrun;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import java.util.HashMap;

import ru.androiddevschool.gsrun.Screens.CharScr;
import ru.androiddevschool.gsrun.Screens.Menu;
import ru.androiddevschool.gsrun.Screens.Play;

public class GSGame extends Game {
	private GSGame(){} //приватный конструктор - больше никто не может использовать кроме меня в этом классе
	private static GSGame instance = new GSGame(); //создание игры - вызов приватного конструктора
	public static GSGame get(){return instance;} //обеспечиваем доступ к экземпляру игры

	private SpriteBatch batch; //отрисовщик
	private HashMap<String, Screen> screens; //хранилище экранов

	@Override
	public void create() {
		//при создании игры
		batch = new SpriteBatch(); // создаю отрисовщик графики
		screens = new HashMap<String, Screen>(); //создаю хранилище экранов
		Gdx.gl.glClearColor(1, 1, 1, 1); //цвет закрашивания экрана во всей игре будет - белый
		screens.put("Menu", new Menu(batch)); //кладу в хранилище экранов экран меню
		screens.put("Character", new CharScr(batch)); //кладу в хранилище экранов экран меню
		screens.put("Play", new Play(batch)); //кладу в хранилище экранов экран меню
		setScreen("Menu");
		 setScreen("Play");
	}

	public void setScreen(String name) {
		if (screens.containsKey(name)) //если существует экран с таким названием
			setScreen(screens.get(name)); //установить его активным
	}
}
