package ru.androiddevschool.gsrun.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.gsrun.Controller.ScreenTraveler;
import ru.androiddevschool.gsrun.Utils.Assets;
import ru.androiddevschool.gsrun.Utils.Values;

/**
 * Created by 01k1102 on 01.04.2017.
 */
public class CharScr extends StdScreen {
    private Image tmpImg;
    public CharScr(SpriteBatch batch) {
        super(batch);
        addUI();
        addBg();
        addStage();
    }

    private void addStage() {
        tmpImg = new Image(Assets.get().images.get("char5-stand"));
        tmpImg.setPosition(Values.WORLD_WIDTH/2-tmpImg.getWidth()/2, Values.ground - 30);
        stage.addActor(tmpImg);
    }

    private void addBg() {
        staticBg.addActor(new Image(Assets.get().images.get("Твой персонаж")));
    }

    private void addUI(){
        Button button;
        Table table = new Table();
        table.setFillParent(true);
        table.setDebug(true);

        table.add().expandX();
        button = new Button(Assets.get().buttonStyles.get("exit button"));
        button.addListener(new ScreenTraveler("Menu"));
        table.add(button).row();

        table.add().colspan(2).expand().row();

        ui.addActor(table);

    }
}
