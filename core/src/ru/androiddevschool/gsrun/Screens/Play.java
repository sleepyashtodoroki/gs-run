package ru.androiddevschool.gsrun.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Timer;

import java.util.Random;

import ru.androiddevschool.gsrun.Controller.ScreenTraveler;
import ru.androiddevschool.gsrun.Model.Fire;
import ru.androiddevschool.gsrun.Model.Player;
import ru.androiddevschool.gsrun.Model.World;
import ru.androiddevschool.gsrun.Utils.Assets;
import ru.androiddevschool.gsrun.Utils.Values;

/**
 * Created by 01k1102 on 01.04.2017.
 */
public class Play extends StdScreen {
    World world;
    Player player;
    Timer timer;
    Label score;

    private final static Random r = new Random();
    public Play(SpriteBatch batch) {
        super(batch);
        addUI();
        addWorld();
    }

    private void addWorld() {
        Image ground = new Image(Assets.get().images.get("земля"));
        ground.setSize(960, Values.ground);
        staticBg.addActor(ground);

        Image sky =  new Image(Assets.get().images.get("небо"));
        sky.setSize(960, 470);
        sky.setPosition(0, Values.ground);
        staticBg.addActor(sky);

        world = new World(stage.getViewport(), stage.getBatch());
        stage = world;
        player = world.getPlayer();

        timer = new Timer();
        timer.scheduleTask(new Timer.Task() {
                               @Override
                               public void run() {
                                   Image image = new Image(Assets.get().images.get("облако"));
                                   image.setSize(r.nextInt(100) + 200, r.nextInt(50) + 100);
                                   image.setPosition(player.getX() + Values.WORLD_WIDTH, Values.WORLD_HEIGHT - image.getHeight() - r.nextInt(30));
                                   world.addActor(image);
                               }
                           },
                2f, //отложение старта
                1f //интервал по времени
        );

        timer.scheduleTask(new Timer.Task() {
                               @Override
                               public void run() {
                                   TextureRegion[] frames = new TextureRegion[2];
                                   for (int i = 0; i < frames.length; i++)
                                       frames[i] = Assets.get().images.get("Огонь " + (i+1)).getRegion();
                                   Fire fire = new Fire(frames, 0.5f, player.getX());
                                   world.addActor(fire);
                               }
                           },
                5f, //отложение старта
                2f //интервал по времени
        );
    }

    private void addUI(){
        Button button;
        Table table = new Table();
        table.setFillParent(true);
        table.setDebug(true);

        score = new Label("", new Label.LabelStyle(Assets.get().fonts.get("score"), null));
        table.add(score).expandX();

        button = new Button(Assets.get().buttonStyles.get("exit button"));
        button.addListener(new ScreenTraveler("Menu"));
        table.add(button).row();

        table.add().colspan(2).expand().row();

        ui.addActor(table);
    }

    protected void postAct() {
        stage.getCamera().position.x = player.getX() - Values.playerX + Values.WORLD_WIDTH/2;
        score.setText("" + world.getScore());
    }

    public void show(){
        super.show();
        timer.start();
    }

    public void hide(){
        super.hide();
        timer.stop();
    }

}
