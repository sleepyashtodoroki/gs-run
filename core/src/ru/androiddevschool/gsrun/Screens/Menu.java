package ru.androiddevschool.gsrun.Screens;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Table;

import ru.androiddevschool.gsrun.Controller.ScreenTraveler;
import ru.androiddevschool.gsrun.Utils.Assets;

/**
 * Created by 01k1102 on 01.04.2017.
 */
public class Menu extends StdScreen {
    public Menu(SpriteBatch batch) {
        super(batch);
        Button button;
        Table table = new Table();
        table.setFillParent(true);

        button = new Button(Assets.get().buttonStyles.get("char button"));
        button.addListener(new ScreenTraveler("Character"));
        table.add(button).padRight(20);

        button = new Button(Assets.get().buttonStyles.get("play button"));
        button.addListener(new ScreenTraveler("Play"));
        table.add(button).padRight(20);



        ui.addActor(table);

        staticBg.addActor(new Image(Assets.get().images.get("Menu main")));
    }
}
